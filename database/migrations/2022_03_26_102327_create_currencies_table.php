<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\CurrencyTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger(Storage::USER_FOREIGN_KEY)->nullable()->comment('ID пользователя');
                $table->string(Storage::COUNTRY, 255)->nullable()->comment('Страна');
                $table->string(Storage::CURRENCY, 255)->comment('Валюта');
                $table->string(Storage::CODE, 255)->comment('Код валюты');
                $table->string(Storage::SYMBOL, 255)->nullable()->comment('Обозначение');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
