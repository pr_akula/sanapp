<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\ProductTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger(Storage::CATEGORY_FOREIGN_KEY)->comment('ID категории');
                $table->unsignedInteger(Storage::COMPANY_FOREIGN_KEY)->comment('ID компании');
                $table->string(Storage::TYPE, 255)->nullable()->comment('Тип');
                $table->integer(Storage::BARCODE)->nullable()->comment('Barcode');
                $table->string(Storage::TITLE, 255)->comment('Наименование');
                $table->integer(Storage::PRICE)->nullable()->comment('Стоимость');
                $table->string(Storage::PRICES, 255)->nullable()->comment('');
                $table->integer(Storage::AMOUNT)->nullable()->comment('Количество');
                $table->string(Storage::IMAGE, 255)->nullable()->comment('Изображение');
                $table->string(Storage::STATUS, 255)->nullable()->comment('Статус');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
