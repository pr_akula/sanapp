<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\DocumentTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger(Storage::USER_FOREIGN_KEY)->comment('ID пользователя');
                $table->unsignedInteger(Storage::WAREHOUSE_PRODUCT_FOREIGN_KEY)
                    ->comment('ID записи с количеством товара на складе');
                $table->integer(Storage::NUMBER)->comment('Номер');
                $table->string(Storage::TYPE, 255)->nullable()->comment('Тип');
                $table->integer(Storage::TOTAL_SUMS)->nullable()->comment('Сумма итого');
                $table->integer(Storage::TOTAL_AMOUNT)->nullable()->comment('Количество итого');
                $table->string(Storage::STATUS, 255)->nullable()->comment('Статус');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
