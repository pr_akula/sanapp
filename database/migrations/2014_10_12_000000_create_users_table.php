<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\UserTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->string(Storage::FIRSTNAME, 255)->nullable()->comment('Имя');
                $table->string(Storage::MIDDLENAME, 255)->nullable()->comment('Отчество');
                $table->string(Storage::LASTNAME, 255)->nullable()->comment('Фамилия');
                $table->string(Storage::EMAIL, 255)->nullable()->comment('E-Mail');
                $table->timestamp(Storage::EMAIL_VERIFIED_AT)->nullable()->comment('E-Mail подтверждён в');
                $table->string(Storage::PASSWORD, 255)->nullable()->comment('Пароль');
                $table->string(Storage::PHONE, 255)->nullable()->comment('Телефон');
                $table->timestamp(Storage::PHONE_VERIFIED_AT)->nullable()->comment('Телефон подтверждён в');
                $table->date(Storage::BIRTHDATE)->nullable()->comment('Дата рождения');
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
