<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\WarehouseProductAmountTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger(Storage::WAREHOUSE_FOREIGN_KEY)->comment('ID склада');
                $table->unsignedInteger(Storage::PRODUCT_FOREIGN_KEY)->comment('ID товара');
                $table->integer(Storage::AMOUNT)->comment('Количество');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
