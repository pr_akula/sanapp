<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\CompanyTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->integer(Storage::BIN)->nullable()->comment('BIN');
                $table->integer(Storage::BIK)->nullable()->comment('BIK');
                $table->string(Storage::NAME, 255)->nullable()->comment('Наименование');
                $table->string(Storage::EMAIL, 255)->nullable()->comment('E-Mail');
                $table->string(Storage::ADDRESS, 255)->nullable()->comment('Адрес');
                $table->integer(Storage::SUPPLIER)->nullable()->comment('Поставщик');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
