<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\UnitTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->string(Storage::REGION, 255)->nullable()->comment('');
                $table->string(Storage::NAME, 255)->comment('Наименование');
                $table->text(Storage::DESCRIPTION)->nullable()->comment('Описание');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
