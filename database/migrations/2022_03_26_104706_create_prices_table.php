<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\PriceTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->string(Storage::TITLE, 255)->nullable()->comment('Наименование');
                $table->decimal(
                    column: Storage::PRICE_COEFFICIENT,
                    total: 10,
                    places: 2,
                    unsigned: true
                )->nullable()->comment('Коэффициент стоимости');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
