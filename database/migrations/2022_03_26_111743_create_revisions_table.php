<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Interfaces\MySQL\RevisionTable as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->id();
                $table->unsignedInteger(Storage::USER_FOREIGN_KEY)->comment('ID пользователя');
                $table->unsignedInteger(Storage::REVISION_PRODUCT_FOREIGN_KEY)->comment('ID ревизии товара');
                $table->string(Storage::COMMENT, 255)->nullable()->comment('Комментарий');
                $table->integer(Storage::REAL_AMOUNT)->nullable()->comment('Реальное количество');
                $table->integer(Storage::CALC_AMOUNT)->nullable()->comment('Вычисленное количество');
                $table->integer(Storage::DIFFERENCE)->nullable()->comment('Разница');
                $table->string(Storage::STATUS, 255)->nullable()->comment('Статус');
                $table->timestamps();
            });
        }
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
