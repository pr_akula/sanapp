<?php

namespace App\Models;

use App\Interfaces\MySQL\CategoryTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model implements CategoryTable
{
    use HasFactory;
}
