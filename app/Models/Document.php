<?php

namespace App\Models;

use App\Interfaces\MySQL\DocumentTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model implements DocumentTable
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(
            related: User::class,
            foreignKey: self::USER_FOREIGN_KEY,
            ownerKey: User::PRIMARY_KEY,
        );
    }

    public function warehouseProduct()
    {
        return $this->belongsTo(
            related: WarehouseProduct::class,
            foreignKey: self::WAREHOUSE_PRODUCT_FOREIGN_KEY,
            ownerKey: WarehouseProduct::PRIMARY_KEY,
        );
    }
}
