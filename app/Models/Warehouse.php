<?php

namespace App\Models;

use App\Interfaces\MySQL\WarehouseProductAmountTable as WarehousesProductsPivot;
use App\Interfaces\MySQL\WarehouseTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model implements WarehouseTable
{
    use HasFactory;

    public function products()
    {
        return $this->belongsToMany(
            related: Product::class,
            table: WarehousesProductsPivot::TABLE_NAME,
            foreignPivotKey: WarehousesProductsPivot::WAREHOUSE_FOREIGN_KEY,
            relatedPivotKey: WarehousesProductsPivot::PRODUCT_FOREIGN_KEY,
        )->withPivot(
            WarehousesProductsPivot::AMOUNT
        )->withTimestamps();
    }
}
