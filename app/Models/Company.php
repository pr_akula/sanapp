<?php

namespace App\Models;

use App\Interfaces\MySQL\CompanyTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model implements CompanyTable
{
    use HasFactory;
}
