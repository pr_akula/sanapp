<?php

namespace App\Models;

use App\Interfaces\MySQL\WarehouseProductAmountTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WarehouseProduct extends Model implements WarehouseProductAmountTable
{
    use HasFactory;

    public function warehouse()
    {
        return $this->belongsTo(
            related: Warehouse::class,
            foreignKey: self::WAREHOUSE_FOREIGN_KEY,
            ownerKey: Warehouse::PRIMARY_KEY,
        );
    }

    public function product()
    {
        return $this->belongsTo(
            related: Product::class,
            foreignKey: self::PRODUCT_FOREIGN_KEY,
            ownerKey: Product::PRIMARY_KEY,
        );
    }
}
