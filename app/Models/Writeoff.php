<?php

namespace App\Models;

use App\Interfaces\MySQL\WriteoffTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Writeoff extends Model implements WriteoffTable
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(
            related: User::class,
            foreignKey: self::USER_FOREIGN_KEY,
            ownerKey: User::PRIMARY_KEY,
        );
    }

    public function product()
    {
        return $this->belongsTo(
            related: Product::class,
            foreignKey: self::PRODUCT_FOREIGN_KEY,
            ownerKey: Product::PRIMARY_KEY,
        );
    }

    public function warehouse()
    {
        return $this->belongsTo(
            related: Warehouse::class,
            foreignKey: self::WAREHOUSE_FOREIGN_KEY,
            ownerKey: Warehouse::PRIMARY_KEY,
        );
    }
}
