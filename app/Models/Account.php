<?php

namespace App\Models;

use App\Interfaces\MySQL\AccountTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model implements AccountTable
{
    use HasFactory;
}
