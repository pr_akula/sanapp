<?php

namespace App\Models;

use App\Interfaces\MySQL\ProductTable;
use App\Interfaces\MySQL\RevisionProductTable as RevisionsProductsPivot;
use App\Interfaces\MySQL\WarehouseProductAmountTable as WarehousesProductsPivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements ProductTable
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo(
            related: Category::class,
            foreignKey: self::CATEGORY_FOREIGN_KEY,
            ownerKey: Category::PRIMARY_KEY,
        );
    }

    public function company()
    {
        return $this->belongsTo(
            related: Company::class,
            foreignKey: self::COMPANY_FOREIGN_KEY,
            ownerKey: Company::PRIMARY_KEY,
        );
    }

    public function revisions()
    {
        return $this->belongsToMany(
            related: Revision::class,
            table: RevisionsProductsPivot::TABLE_NAME,
            foreignPivotKey: RevisionsProductsPivot::PRODUCT_FOREIGN_KEY,
            relatedPivotKey: RevisionsProductsPivot::REVISION_FOREIGN_KEY,
        );
    }

    public function warehouses()
    {
        return $this->belongsToMany(
            related: Warehouse::class,
            table: WarehousesProductsPivot::TABLE_NAME,
            foreignPivotKey: WarehousesProductsPivot::PRODUCT_FOREIGN_KEY,
            relatedPivotKey: WarehousesProductsPivot::WAREHOUSE_FOREIGN_KEY,
        )->withPivot(
            WarehousesProductsPivot::AMOUNT
        )->withTimestamps();
    }
}
