<?php

namespace App\Models;

use App\Interfaces\MySQL\UnitTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model implements UnitTable
{
    use HasFactory;
}
