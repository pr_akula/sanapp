<?php

namespace App\Models;

use App\Interfaces\MySQL\PriceTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model implements PriceTable
{
    use HasFactory;
}
