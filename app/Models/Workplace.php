<?php

namespace App\Models;

use App\Interfaces\MySQL\WorkplaceTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Workplace extends Model implements WorkplaceTable
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(
            related: User::class,
            foreignKey: self::USER_FOREIGN_KEY,
            ownerKey: User::PRIMARY_KEY,
        );
    }
}
