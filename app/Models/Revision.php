<?php

namespace App\Models;

use App\Interfaces\MySQL\RevisionProductTable as RevisionsProductsPivot;
use App\Interfaces\MySQL\RevisionTable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revision extends Model implements RevisionTable
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(
            related: User::class,
            foreignKey: self::USER_FOREIGN_KEY,
            ownerKey: User::PRIMARY_KEY,
        );
    }

    public function products()
    {
        return $this->belongsToMany(
            related: Product::class,
            table: RevisionsProductsPivot::TABLE_NAME,
            foreignPivotKey: RevisionsProductsPivot::REVISION_FOREIGN_KEY,
            relatedPivotKey: RevisionsProductsPivot::PRODUCT_FOREIGN_KEY,
        );
    }
}
