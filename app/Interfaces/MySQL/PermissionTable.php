<?php

namespace App\Interfaces\MySQL;

interface PermissionTable
{
    public const TABLE_NAME                 = 'permissions';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
}
