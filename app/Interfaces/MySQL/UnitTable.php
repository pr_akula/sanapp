<?php

namespace App\Interfaces\MySQL;

interface UnitTable
{
    public const TABLE_NAME                     = 'revisions';

    public const PRIMARY_KEY                    = self::ID;

    public const ID                             = 'id';
    public const REGION                         = 'region';
    public const NAME                           = 'name';
    public const DESCRIPTION                    = 'description';
}
