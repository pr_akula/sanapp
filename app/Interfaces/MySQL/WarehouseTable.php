<?php

namespace App\Interfaces\MySQL;

interface WarehouseTable
{
    public const TABLE_NAME                 = 'warehouses';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
    public const ADDRESS                    = 'address';
    public const STATUS                     = 'status';
}
