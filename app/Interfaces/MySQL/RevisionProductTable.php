<?php

namespace App\Interfaces\MySQL;

interface RevisionProductTable
{
    public const TABLE_NAME                     = 'revisions_products';

    public const PRIMARY_KEY                    = self::ID;
    public const REVISION_FOREIGN_KEY           = self::REVISION_ID;
    public const PRODUCT_FOREIGN_KEY            = self::PRODUCT_ID;

    public const ID                             = 'id';
    public const REVISION_ID                    = 'revision_id';
    public const PRODUCT_ID                     = 'product_id';
}
