<?php

namespace App\Interfaces\MySQL;

interface ProductTable
{
    public const TABLE_NAME                 = 'products';

    public const PRIMARY_KEY                = self::ID;
    public const CATEGORY_FOREIGN_KEY       = self::CATEGORY_ID;
    public const COMPANY_FOREIGN_KEY        = self::COMPANY_ID;

    public const ID                         = 'id';
    public const CATEGORY_ID                = 'category_id';
    public const COMPANY_ID                 = 'company_id';
    public const TYPE                       = 'type';
    public const BARCODE                    = 'barcode';
    public const TITLE                      = 'title';
    public const PRICE                      = 'price';
    public const PRICES                     = 'prices';
    public const AMOUNT                     = 'amount';
    public const IMAGE                      = 'image';
    public const STATUS                     = 'status';
}
