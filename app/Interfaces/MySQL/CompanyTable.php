<?php

namespace App\Interfaces\MySQL;

interface CompanyTable
{
    public const TABLE_NAME                 = 'companies';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const BIN                        = 'bin';
    public const BIK                        = 'bik';
    public const NAME                       = 'name';
    public const EMAIL                      = 'email';
    public const ADDRESS                    = 'address';
    public const SUPPLIER                   = 'supplier';
}
