<?php

namespace App\Interfaces\MySQL;

interface WorkplaceTable
{
    public const TABLE_NAME                 = 'workplaces';

    public const PRIMARY_KEY                = self::ID;
    public const USER_FOREIGN_KEY           = self::USER_ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
    public const STATUS                     = 'status';
    public const USER_ID                    = 'user_id';
}
