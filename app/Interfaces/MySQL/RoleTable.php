<?php

namespace App\Interfaces\MySQL;

interface RoleTable
{
    public const TABLE_NAME                 = 'roles';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
    public const DISPLAY_NAME               = 'display_name';
}
