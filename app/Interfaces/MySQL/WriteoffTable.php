<?php

namespace App\Interfaces\MySQL;

interface WriteoffTable
{
    public const TABLE_NAME                     = 'writeoff';

    public const PRIMARY_KEY                    = self::ID;
    public const USER_FOREIGN_KEY               = self::USER_ID;
    public const WAREHOUSE_FOREIGN_KEY          = self::WAREHOUSE_ID;
    public const PRODUCT_FOREIGN_KEY            = self::PRODUCT_ID;

    public const ID                             = 'id';
    public const USER_ID                        = 'user_id';
    public const WAREHOUSE_ID                   = 'warehouse_id';
    public const PRODUCT_ID                     = 'product_id';
    public const COMMENT                        = 'comment';
    public const TOTAL_AMOUNT                   = 'total_amount';
    public const TOTAL_SUMS                     = 'total_sums';
    public const STATUS                         = 'status';
}
