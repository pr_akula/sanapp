<?php

namespace App\Interfaces\MySQL;

interface CategoryTable
{
    public const TABLE_NAME                 = 'categories';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const TITLE                      = 'title';
    public const DESCRIPTION                = 'description';
    public const STATUS                     = 'status';
}
