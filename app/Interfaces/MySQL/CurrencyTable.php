<?php

namespace App\Interfaces\MySQL;

interface CurrencyTable
{
    public const TABLE_NAME                 = 'currencies';

    public const PRIMARY_KEY                = self::ID;
    public const USER_FOREIGN_KEY           = self::USER_ID;

    public const ID                         = 'id';
    public const USER_ID                    = 'user_id';
    public const COUNTRY                    = 'country';
    public const CURRENCY                   = 'currency';
    public const CODE                       = 'code';
    public const SYMBOL                     = 'symbol';
}
