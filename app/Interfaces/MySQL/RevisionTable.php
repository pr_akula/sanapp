<?php

namespace App\Interfaces\MySQL;

interface RevisionTable
{
    public const TABLE_NAME                     = 'revisions';

    public const PRIMARY_KEY                    = self::ID;
    public const USER_FOREIGN_KEY               = self::USER_ID;
    public const REVISION_PRODUCT_FOREIGN_KEY   = self::REVISION_PRODUCT_ID;

    public const ID                             = 'id';
    public const USER_ID                        = 'user_id';
    public const REVISION_PRODUCT_ID            = 'revision_product_id';
    public const COMMENT                        = 'comment';
    public const REAL_AMOUNT                    = 'real_amount';
    public const CALC_AMOUNT                    = 'calc_amount';
    public const DIFFERENCE                     = 'difference';
    public const STATUS                         = 'status';
}
