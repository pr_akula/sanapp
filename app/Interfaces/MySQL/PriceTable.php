<?php

namespace App\Interfaces\MySQL;

interface PriceTable
{
    public const TABLE_NAME                 = 'prices';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const TITLE                      = 'title';
    public const PRICE_COEFFICIENT          = 'price_coefficient';
}
