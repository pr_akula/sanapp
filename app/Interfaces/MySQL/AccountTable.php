<?php

namespace App\Interfaces\MySQL;

interface AccountTable
{
    public const TABLE_NAME                 = 'accounts';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const NAME                       = 'name';
    public const NUMBER                     = 'number';
    public const BANK                       = 'bank';
}
