<?php

namespace App\Interfaces\MySQL;

interface DocumentTable
{
    public const TABLE_NAME                     = 'documents'; /**  Waybill, Overhead, Check */

    public const PRIMARY_KEY                    = self::ID;
    public const USER_FOREIGN_KEY               = self::USER_ID;
    public const WAREHOUSE_PRODUCT_FOREIGN_KEY  = self::WAREHOUSE_PRODUCT_ID;

    public const ID                             = 'id';
    public const USER_ID                        = 'user_id';
    public const WAREHOUSE_PRODUCT_ID           = 'warehouse_product_id';
    public const TYPE                           = 'type';
    public const NUMBER                         = 'number';
    public const TOTAL_AMOUNT                   = 'total_amount';
    public const TOTAL_SUMS                     = 'total_sums';
    public const STATUS                         = 'status';
}
