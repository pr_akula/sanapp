<?php

namespace App\Interfaces\MySQL;

interface WarehouseProductAmountTable
{
    public const TABLE_NAME                     = 'warehouses_products_amounts';

    public const PRIMARY_KEY                    = self::ID;
    public const WAREHOUSE_FOREIGN_KEY          = self::WAREHOUSE_ID;
    public const PRODUCT_FOREIGN_KEY            = self::PRODUCT_ID;

    public const ID                             = 'id';
    public const WAREHOUSE_ID                   = 'revision_id';
    public const PRODUCT_ID                     = 'product_id';
    public const AMOUNT                         = 'amount';
}
