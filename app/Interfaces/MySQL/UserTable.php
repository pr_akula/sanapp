<?php

namespace App\Interfaces\MySQL;

interface UserTable
{
    public const TABLE_NAME                 = 'users';

    public const PRIMARY_KEY                = self::ID;

    public const ID                         = 'id';
    public const EMAIL                      = 'email';
    public const EMAIL_VERIFIED_AT          = 'email_verified_at';
    public const PASSWORD                   = 'password';
    public const PHONE                      = 'phone';
    public const PHONE_VERIFIED_AT          = 'phone_verified_at';
    public const REMEMBER_TOKEN             = 'remember_token';

    public const FIRSTNAME                  = 'firstname';
    public const MIDDLENAME                 = 'middlename';
    public const LASTNAME                   = 'lastname';
    public const BIRTHDATE                  = 'birthdate';
}
